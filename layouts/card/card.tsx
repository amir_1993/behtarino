import React, { ReactElement } from "react";
import classes from "./card.module.scss";

interface cardProps {
  children: ReactElement;
}
const Card = ({ children }: cardProps) => {
  return <div className={classes.card}>{children}</div>;
};
export default Card;
