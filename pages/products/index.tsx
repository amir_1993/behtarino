import { GetServerSideProps } from "next";
import Head from "next/head";
import { Fragment } from "react";
import ProductsCard from "../../components/products/productsCard/productsCard";
import { ProductsProps } from "../../types/products";

const Products = ({ Plist }: ProductsProps) => {
  return (
    <Fragment>
      <Head>
        <title>product's title</title>
        <meta name="description" content="product's description" />
      </Head>
      {Plist.map((item) => (
        <ProductsCard {...item} key={item.id} />
      ))}
    </Fragment>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  try {
    const res = await fetch(`${process.env.NEXT_APP_BASE_URL}/products`);
    const Plist = await res.json();
    return {
      props: {
        Plist,
      },
    };
  } catch (error) {
    return { notFound: true };
  }
};
export default Products;
