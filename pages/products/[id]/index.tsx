import React from "react";
import ProductsDetails from "../../../components/products/productsDetails/productsDetails";
import { GetServerSideProps } from "next";
import { ProductsProps } from "../../../types/products";

type PDetail = { PDetail: ProductsProps["Plist"][0] };

const ProductDetails = ({ PDetail }: PDetail) => {
  return <ProductsDetails {...PDetail} />;
};
export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const res = await fetch(
      `${process.env.NEXT_APP_BASE_URL}/products/${context?.params?.id}`
    );
    const PDetail = await res.json();
    return {
      props: {
        PDetail,
      },
    };
  } catch (error) {
    return { notFound: true };
  }
};
export default ProductDetails;
