import Products from "./products";
import { useRouter } from "next/router";
import { useEffect } from "react";

const Home = () => {
  const router = useRouter();

  useEffect(() => {
    router.push({
      pathname: "/products",
    });
  }, []);

  return <Products Plist={[]} />;
};

export default Home;
