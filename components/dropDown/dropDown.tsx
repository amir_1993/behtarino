import React from "react";
import classes from "./dropDown.module.scss";
interface DropDownProps {
  options: string[];
  label: string;
}
const DropDown = ({ options, label }: DropDownProps) => {
  return (
    <div>
      <label className={classes.label} htmlFor="size">
        {label}
      </label>
      <select className={classes.select}>
        {options.map((item, index) => (
          <option value="1" key={index}>
            {item}
          </option>
        ))}
      </select>
    </div>
  );
};
export default DropDown;
