import { ReactElement } from "react";
import classes from "./center.module.scss";

interface centerProps {
  children: ReactElement;
}
const Center = ({ children }: centerProps) => {
  return <div className={classes.center}>{children}</div>;
};
export default Center;
