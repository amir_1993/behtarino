import React, { memo, useState } from "react";
import classes from "./productColors.module.scss";
interface productColorsProps {
  colors: Array<{ color: string; id: string }>;
}
const ProductColors = ({ colors }: productColorsProps) => {
  const [checked, setChecked] = useState<any>({ [colors[0].color]: true });
  const checkedColor = (id: string) => {
    setChecked((prev: any) => {
      if (prev && prev[id] !== undefined) {
        prev = { [id]: !prev[id] };
      } else if (!prev) {
        prev = { [id]: true };
      } else if (prev && prev[id] === undefined) {
        prev = { [id]: true };
      }
      return prev;
    });
  };

  return (
    <div>
      <label className={classes.title}>COLOR</label>
      <div className={classes.container}>
        {colors.map((item) => (
          <div
            key={item.id}
            className={classes.colors}
            style={{ backgroundColor: item.color }}
            onClick={() => checkedColor(item.id)}
          >
            {checked && checked[item.id] && (
              <div
                style={{ border: `3px solid ${item.color}` }}
                className={classes.checked}
              ></div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};
export default memo(ProductColors);
