import React from "react";
import classes from "./basketButton.module.scss";
import basketIcon from "./assets/fastBuy.svg";
import Image from "next/image";

interface BasketButtonProps {
  title: string;
}
const BasketButton = ({ title }: BasketButtonProps) => {
  return (
    <button className={classes.basketButton}>
      <Image src={basketIcon} alt="buy" width={30} height={30} />
      <span>{title}</span>
    </button>
  );
};
export default BasketButton;
