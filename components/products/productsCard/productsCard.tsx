import React from "react";
import Card from "../../../layouts/card/card";
import { ProductsProps } from "../../../types/products";
import Center from "../../center/center";
import classes from "./productsCard.module.scss";
import { Rating } from "react-simple-star-rating";
import Link from "next/link";

type item = ProductsProps["Plist"][0];

const ProductsCard = (item: item) => {
  return (
    <Center>
      <Card>
        <div className={classes.product}>
          <div className={classes.product__leftSide}>
            <span className={classes.product__category}>{item.category}</span>

            <p className={classes.product__title}>
              {item.title}{" "}
              <span className={classes.product__price}>{`${item.price}$`}</span>
            </p>

            <span className={classes.product__description}>
              {item.description}
            </span>
            <div>
              <Rating
                ratingValue={(item.rating.rate * 100) / 5}
                size={20}
                readonly
              />
              <span className={classes.product__rateCount}>
                ({item.rating.count})
              </span>
            </div>
          </div>
          <Link href={`/products/${item.id}`}>
            <img src={item.image} alt="photo" loading={"lazy"} />
          </Link>
        </div>
      </Card>
    </Center>
  );
};
export default ProductsCard;
