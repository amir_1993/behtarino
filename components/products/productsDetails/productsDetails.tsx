import Image from "next/image";
import React from "react";
import { Rating } from "react-simple-star-rating";
import Card from "../../../layouts/card/card";
import { ProductsProps } from "../../../types/products";
import BasketButton from "../../basketButton/basketButton";
import DropDown from "../../dropDown/dropDown";
import ProductColors from "../../productColors/productColors";
import classes from "./productsDetails.module.scss";
import shareIcon from "./assets/share.svg";
import Head from "next/head";
type PDetail = ProductsProps["Plist"][0];
const colors = [
  { color: "green", id: "green" },
  { color: "yellow", id: "yellow" },
  { color: "red", id: "red" },
  { color: "black", id: "black" },
  { color: "gray", id: "gray" },
];

const ProductsDetails = (PDetail: PDetail) => {
  return (
    <div className={classes.container}>
      <Head>
        <title>product's title</title>
        <meta name="description" content="product's description" />
      </Head>
      <Card>
        <div className={classes.card_items_container}>
          <img
            className={classes.Image}
            loading={"lazy"}
            src={PDetail.image}
            alt="photo"
          />
          <div>
            <div className={classes.title_rating}>
              <span className={classes.title}>{PDetail.title}</span>
              <Rating
                ratingValue={(PDetail.rating.rate * 100) / 5}
                size={20}
                readonly
              />
            </div>
            <p className={classes.price}>{`${PDetail.price}$`}</p>
            <p className={classes.description_title}>DESCRIPTION</p>
            <p className={classes.description}>{PDetail.description}</p>
            <div className={classes.typeContainer}>
              <ProductColors colors={colors} />
              <div className={classes.verticalLine}></div>
              <DropDown options={["(UK 8)", "(UK 7)"]} label={"SIZE"} />
              <div className={classes.verticalLine}></div>

              <DropDown options={["(1)", "(2)"]} label={"QTY"} />
            </div>
            <div className={classes.share_and_btn}>
              <BasketButton title="ADD TO CART" />
              <Image src={shareIcon} alt="share" />
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default ProductsDetails;
